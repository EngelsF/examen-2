﻿using NETProjectTutorial.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.model
{
    class ClienteModel
    {
        private static List<Cliente> ListClientes = new List<Cliente>();
        private implements.DaoImplementCliente daocliente;

        public ClienteModel()
        {
            daocliente = new implements.DaoImplementCliente();
        }

        public List<Cliente> GetListCliente()
        {
            return daocliente.findAll();
        }

        public static void Populate()
        {
            ListClientes = JsonConvert.DeserializeObject<List<Cliente>>(System.Text.Encoding.Default.GetString(NETProjectTutorial.Properties.Resources.Cliente_data));
            foreach(Cliente c int ListClientes)
                {
                daocliente.save(c);
            }
        }

        public void save(DataRow cliente)
        {
            Cliente c = new Cliente();
            c.Id = Convert.ToInt32(cliente["Id"].ToString());
            c.Cedula = cliente["Cedula"].ToString();
            c.Nombres = cliente["Nombres"].ToString();
            c.Apellidos = cliente["Apellidos"].ToString();
            c.Telefono = cliente["Telefono"].ToString();
            c.Correo = cliente["Correo"].ToString();
            c.Direccion = cliente["Direccion"].ToString();

            daocliente.save();
        }

    }
}
