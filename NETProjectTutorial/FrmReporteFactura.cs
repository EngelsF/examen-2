﻿using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmReporteFactura : Form
    {
        private DataSet dsSistema;
        private DataRow drFactura;

        public FrmReporteFactura()
        {
            InitializeComponent();
        }

        public DataSet DsSistema
        {
            get
            {
                return dsSistema;
            }

            set
            {
                dsSistema = value;
            }
        }

        public DataRow DrFactura
        {
            set
            {
                drFactura = value;
            }
        }

        private void FrmReporteFactura_Load(object sender, EventArgs e)
        {
            dsSistema.Tables["ReporteFactura"].Rows.Clear();

            DataTable dtFactura = dsSistema.Tables["Factura"];
            int countFactura = dtFactura.Rows.Count;

            DataRow drFactura = dtFactura.Rows[countFactura - 1];
            DataRow drEmpleado = dsSistema.Tables["Empleado"].Rows.Find(drFactura["Empleado"]);

            DataRow drCliente = dsSistema.Tables["Cliente"].Rows.Find(drFactura["Cliente"]);

            DataTable dtDetalleFactura = dsSistema.Tables["DetalleFactura"];

            DataRow [] drDetallesFacturas = 
                dtDetalleFactura.Select(String.Format("Factura = {0}",
                drFactura["Id"]));
            foreach(DataRow dr in drDetallesFacturas)
            {
                DataRow drReporteFactura = dsSistema.Tables["ReporteFactura"].NewRow();
                DataRow drProducto = dsSistema.Tables["Producto"].Rows.Find(dr["Producto"]);
                drReporteFactura["Cod_Factura"] = drFactura["CodFactura"];
                drReporteFactura["Fecha"] = drFactura["Fecha"];
                drReporteFactura["Subtotal"] = drFactura["Subtotal"];
                drReporteFactura["IVA"] = drFactura["Iva"];
                drReporteFactura["Total"] = drFactura["Total"];
                drReporteFactura["Nombre_Empleado"] = drEmpleado["Nombres"];
                drReporteFactura["Apellido_Empleado"] = drEmpleado["Apellidos"];
                drReporteFactura["SKU"] = drProducto["SKU"];
                drReporteFactura["Nombre_Producto"] = drProducto["Nombre"];
                drReporteFactura["Cantidad"] = dr["Cantidad"];
                drReporteFactura["Precio"] = dr["Precio"];
                drReporteFactura["Nombre_Cliente"] = dr["Cliente"];

                dsSistema.Tables["ReporteFactura"].Rows.Add(drReporteFactura);
            }

            this.reportViewer1.LocalReport.ReportEmbeddedResource = "NETProjectTutorial.ReporteFactura.rdlc";
            ReportDataSource rds1 = new ReportDataSource("dsReporteFactura", dsSistema.Tables["ReporteFactura"]);            
            this.reportViewer1.LocalReport.DataSources.Clear();
            this.reportViewer1.LocalReport.DataSources.Add(rds1);
            

            this.reportViewer1.RefreshReport();
        }
    }
}
