﻿namespace NETProjectTutorial
{
    partial class FrmMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.catalogoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.empleadosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.FacturasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clientesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.salirToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.facturaciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.nuevaFacturaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dsProductos = new System.Data.DataSet();
            this.tblProductos = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn31 = new System.Data.DataColumn();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dataColumn30 = new System.Data.DataColumn();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.dataColumn23 = new System.Data.DataColumn();
            this.dataColumn24 = new System.Data.DataColumn();
            this.dataColumn57 = new System.Data.DataColumn();
            this.dataTable3 = new System.Data.DataTable();
            this.dataColumn25 = new System.Data.DataColumn();
            this.dataColumn26 = new System.Data.DataColumn();
            this.dataColumn27 = new System.Data.DataColumn();
            this.dataColumn28 = new System.Data.DataColumn();
            this.dataColumn29 = new System.Data.DataColumn();
            this.dataColumn55 = new System.Data.DataColumn();
            this.dataTable4 = new System.Data.DataTable();
            this.dataColumn32 = new System.Data.DataColumn();
            this.dataColumn33 = new System.Data.DataColumn();
            this.dataColumn34 = new System.Data.DataColumn();
            this.dataColumn35 = new System.Data.DataColumn();
            this.dataColumn36 = new System.Data.DataColumn();
            this.dataTable5 = new System.Data.DataTable();
            this.dataColumn37 = new System.Data.DataColumn();
            this.dataColumn38 = new System.Data.DataColumn();
            this.dataColumn39 = new System.Data.DataColumn();
            this.dataColumn40 = new System.Data.DataColumn();
            this.dataColumn41 = new System.Data.DataColumn();
            this.dataColumn42 = new System.Data.DataColumn();
            this.dataColumn43 = new System.Data.DataColumn();
            this.dataColumn44 = new System.Data.DataColumn();
            this.dataColumn45 = new System.Data.DataColumn();
            this.dataColumn46 = new System.Data.DataColumn();
            this.dataColumn47 = new System.Data.DataColumn();
            this.dataColumn56 = new System.Data.DataColumn();
            this.dataTable6 = new System.Data.DataTable();
            this.dataColumn48 = new System.Data.DataColumn();
            this.dataColumn49 = new System.Data.DataColumn();
            this.dataColumn50 = new System.Data.DataColumn();
            this.dataColumn51 = new System.Data.DataColumn();
            this.dataColumn52 = new System.Data.DataColumn();
            this.dataColumn53 = new System.Data.DataColumn();
            this.dataColumn54 = new System.Data.DataColumn();
            this.bsProductos = new System.Windows.Forms.BindingSource(this.components);
            this.dataColumn58 = new System.Data.DataColumn();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsProductos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblProductos)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsProductos)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.catalogoToolStripMenuItem,
            this.facturaciónToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(635, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // catalogoToolStripMenuItem
            // 
            this.catalogoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.productosToolStripMenuItem,
            this.empleadosToolStripMenuItem,
            this.FacturasToolStripMenuItem,
            this.clientesToolStripMenuItem,
            this.toolStripSeparator1,
            this.salirToolStripMenuItem});
            this.catalogoToolStripMenuItem.Name = "catalogoToolStripMenuItem";
            this.catalogoToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.catalogoToolStripMenuItem.Text = "Catálogos";
            // 
            // productosToolStripMenuItem
            // 
            this.productosToolStripMenuItem.Image = global::NETProjectTutorial.Properties.Resources.products;
            this.productosToolStripMenuItem.Name = "productosToolStripMenuItem";
            this.productosToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.productosToolStripMenuItem.Text = "Productos";
            this.productosToolStripMenuItem.Click += new System.EventHandler(this.productosToolStripMenuItem_Click);
            // 
            // empleadosToolStripMenuItem
            // 
            this.empleadosToolStripMenuItem.Image = global::NETProjectTutorial.Properties.Resources.employee;
            this.empleadosToolStripMenuItem.Name = "empleadosToolStripMenuItem";
            this.empleadosToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.empleadosToolStripMenuItem.Text = "Empleados";
            this.empleadosToolStripMenuItem.Click += new System.EventHandler(this.EmpleadosToolStripMenuItem_Click);
            // 
            // FacturasToolStripMenuItem
            // 
            this.FacturasToolStripMenuItem.Image = global::NETProjectTutorial.Properties.Resources.factura;
            this.FacturasToolStripMenuItem.Name = "FacturasToolStripMenuItem";
            this.FacturasToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.FacturasToolStripMenuItem.Text = "Facturas";
            this.FacturasToolStripMenuItem.Click += new System.EventHandler(this.FacturasToolStripMenuItem_Click);
            // 
            // clientesToolStripMenuItem
            // 
            this.clientesToolStripMenuItem.Image = global::NETProjectTutorial.Properties.Resources.specialist_user;
            this.clientesToolStripMenuItem.Name = "clientesToolStripMenuItem";
            this.clientesToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.clientesToolStripMenuItem.Text = "Clientes";
            this.clientesToolStripMenuItem.Click += new System.EventHandler(this.ClientesToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(129, 6);
            // 
            // salirToolStripMenuItem
            // 
            this.salirToolStripMenuItem.Name = "salirToolStripMenuItem";
            this.salirToolStripMenuItem.Size = new System.Drawing.Size(132, 22);
            this.salirToolStripMenuItem.Text = "Salir";
            // 
            // facturaciónToolStripMenuItem
            // 
            this.facturaciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.nuevaFacturaToolStripMenuItem});
            this.facturaciónToolStripMenuItem.Name = "facturaciónToolStripMenuItem";
            this.facturaciónToolStripMenuItem.Size = new System.Drawing.Size(81, 20);
            this.facturaciónToolStripMenuItem.Text = "Facturación";
            // 
            // nuevaFacturaToolStripMenuItem
            // 
            this.nuevaFacturaToolStripMenuItem.Image = global::NETProjectTutorial.Properties.Resources.factura;
            this.nuevaFacturaToolStripMenuItem.Name = "nuevaFacturaToolStripMenuItem";
            this.nuevaFacturaToolStripMenuItem.Size = new System.Drawing.Size(150, 22);
            this.nuevaFacturaToolStripMenuItem.Text = "Nueva Factura";
            this.nuevaFacturaToolStripMenuItem.Click += new System.EventHandler(this.NuevaFacturaToolStripMenuItem_Click);
            // 
            // dsProductos
            // 
            this.dsProductos.DataSetName = "NewDataSet";
            this.dsProductos.Tables.AddRange(new System.Data.DataTable[] {
            this.tblProductos,
            this.dataTable1,
            this.dataTable2,
            this.dataTable3,
            this.dataTable4,
            this.dataTable5,
            this.dataTable6});
            // 
            // tblProductos
            // 
            this.tblProductos.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn31});
            this.tblProductos.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
            this.tblProductos.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn1};
            this.tblProductos.TableName = "Producto";
            // 
            // dataColumn1
            // 
            this.dataColumn1.AllowDBNull = false;
            this.dataColumn1.ColumnName = "Id";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "SKU";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "Nombre";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "Descripcion";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "Cantidad";
            this.dataColumn5.DataType = typeof(int);
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "Precio";
            this.dataColumn6.DataType = typeof(double);
            // 
            // dataColumn31
            // 
            this.dataColumn31.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn31.ColumnName = "SKUN";
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9,
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13,
            this.dataColumn14,
            this.dataColumn15,
            this.dataColumn16,
            this.dataColumn30});
            this.dataTable1.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
            this.dataTable1.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn7};
            this.dataTable1.TableName = "Empleado";
            // 
            // dataColumn7
            // 
            this.dataColumn7.AllowDBNull = false;
            this.dataColumn7.ColumnName = "Id";
            this.dataColumn7.DataType = typeof(int);
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "INSS";
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "Cédula";
            // 
            // dataColumn10
            // 
            this.dataColumn10.ColumnName = "Nombres";
            // 
            // dataColumn11
            // 
            this.dataColumn11.ColumnName = "Apellidos";
            // 
            // dataColumn12
            // 
            this.dataColumn12.ColumnName = "Dirección";
            // 
            // dataColumn13
            // 
            this.dataColumn13.ColumnName = "Teléfono";
            // 
            // dataColumn14
            // 
            this.dataColumn14.ColumnName = "Celular";
            // 
            // dataColumn15
            // 
            this.dataColumn15.ColumnName = "Salario";
            this.dataColumn15.DataType = typeof(double);
            // 
            // dataColumn16
            // 
            this.dataColumn16.ColumnName = "Sexo";
            // 
            // dataColumn30
            // 
            this.dataColumn30.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn30.ColumnName = "NA";
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn17,
            this.dataColumn18,
            this.dataColumn19,
            this.dataColumn20,
            this.dataColumn21,
            this.dataColumn22,
            this.dataColumn23,
            this.dataColumn24,
            this.dataColumn57});
            this.dataTable2.TableName = "Factura";
            // 
            // dataColumn17
            // 
            this.dataColumn17.AutoIncrement = true;
            this.dataColumn17.ColumnName = "Id";
            this.dataColumn17.DataType = typeof(int);
            // 
            // dataColumn18
            // 
            this.dataColumn18.ColumnName = "CodFactura";
            // 
            // dataColumn19
            // 
            this.dataColumn19.ColumnName = "Fecha";
            this.dataColumn19.DataType = typeof(System.DateTime);
            // 
            // dataColumn20
            // 
            this.dataColumn20.ColumnName = "Observaciones";
            // 
            // dataColumn21
            // 
            this.dataColumn21.ColumnName = "Empleado";
            this.dataColumn21.DataType = typeof(int);
            // 
            // dataColumn22
            // 
            this.dataColumn22.ColumnName = "SubTotal";
            this.dataColumn22.DataType = typeof(double);
            // 
            // dataColumn23
            // 
            this.dataColumn23.ColumnName = "IVA";
            this.dataColumn23.DataType = typeof(double);
            // 
            // dataColumn24
            // 
            this.dataColumn24.ColumnName = "Total";
            this.dataColumn24.DataType = typeof(double);
            // 
            // dataColumn57
            // 
            this.dataColumn57.ColumnName = "Cliente";
            // 
            // dataTable3
            // 
            this.dataTable3.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn25,
            this.dataColumn26,
            this.dataColumn27,
            this.dataColumn28,
            this.dataColumn29,
            this.dataColumn55});
            this.dataTable3.TableName = "DetalleFactura";
            // 
            // dataColumn25
            // 
            this.dataColumn25.AutoIncrement = true;
            this.dataColumn25.ColumnName = "Id";
            this.dataColumn25.DataType = typeof(int);
            // 
            // dataColumn26
            // 
            this.dataColumn26.ColumnName = "Factura";
            this.dataColumn26.DataType = typeof(int);
            // 
            // dataColumn27
            // 
            this.dataColumn27.ColumnName = "Producto";
            this.dataColumn27.DataType = typeof(int);
            // 
            // dataColumn28
            // 
            this.dataColumn28.ColumnName = "Cantidad";
            this.dataColumn28.DataType = typeof(int);
            // 
            // dataColumn29
            // 
            this.dataColumn29.ColumnName = "Precio";
            this.dataColumn29.DataType = typeof(double);
            // 
            // dataColumn55
            // 
            this.dataColumn55.ColumnName = "Cliente";
            // 
            // dataTable4
            // 
            this.dataTable4.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn32,
            this.dataColumn33,
            this.dataColumn34,
            this.dataColumn35,
            this.dataColumn36});
            this.dataTable4.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "SKU"}, false)});
            this.dataTable4.TableName = "ProductoFactura";
            // 
            // dataColumn32
            // 
            this.dataColumn32.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn32.ColumnName = "Id";
            this.dataColumn32.DataType = typeof(int);
            // 
            // dataColumn33
            // 
            this.dataColumn33.ColumnName = "SKU";
            this.dataColumn33.ReadOnly = true;
            // 
            // dataColumn34
            // 
            this.dataColumn34.ColumnName = "Nombre";
            this.dataColumn34.ReadOnly = true;
            // 
            // dataColumn35
            // 
            this.dataColumn35.ColumnName = "Cantidad";
            this.dataColumn35.DataType = typeof(int);
            // 
            // dataColumn36
            // 
            this.dataColumn36.ColumnName = "Precio";
            this.dataColumn36.DataType = typeof(double);
            this.dataColumn36.ReadOnly = true;
            // 
            // dataTable5
            // 
            this.dataTable5.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn37,
            this.dataColumn38,
            this.dataColumn39,
            this.dataColumn40,
            this.dataColumn41,
            this.dataColumn42,
            this.dataColumn43,
            this.dataColumn44,
            this.dataColumn45,
            this.dataColumn46,
            this.dataColumn47,
            this.dataColumn56});
            this.dataTable5.TableName = "ReporteFactura";
            // 
            // dataColumn37
            // 
            this.dataColumn37.ColumnName = "Cod_Factura";
            // 
            // dataColumn38
            // 
            this.dataColumn38.ColumnName = "Fecha";
            this.dataColumn38.DataType = typeof(System.DateTime);
            // 
            // dataColumn39
            // 
            this.dataColumn39.ColumnName = "Subtotal";
            this.dataColumn39.DataType = typeof(double);
            // 
            // dataColumn40
            // 
            this.dataColumn40.ColumnName = "IVA";
            this.dataColumn40.DataType = typeof(double);
            // 
            // dataColumn41
            // 
            this.dataColumn41.ColumnName = "Total";
            this.dataColumn41.DataType = typeof(double);
            // 
            // dataColumn42
            // 
            this.dataColumn42.ColumnName = "Cantidad";
            this.dataColumn42.DataType = typeof(int);
            // 
            // dataColumn43
            // 
            this.dataColumn43.ColumnName = "Precio";
            this.dataColumn43.DataType = typeof(double);
            // 
            // dataColumn44
            // 
            this.dataColumn44.ColumnName = "SKU";
            // 
            // dataColumn45
            // 
            this.dataColumn45.ColumnName = "Nombre_Producto";
            // 
            // dataColumn46
            // 
            this.dataColumn46.ColumnName = "Nombre_Empleado";
            // 
            // dataColumn47
            // 
            this.dataColumn47.ColumnName = "Apellido_Empleado";
            // 
            // dataColumn56
            // 
            this.dataColumn56.ColumnName = "Nombre_Cliente";
            // 
            // dataTable6
            // 
            this.dataTable6.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn48,
            this.dataColumn49,
            this.dataColumn50,
            this.dataColumn51,
            this.dataColumn52,
            this.dataColumn53,
            this.dataColumn54,
            this.dataColumn58});
            this.dataTable6.Constraints.AddRange(new System.Data.Constraint[] {
            new System.Data.UniqueConstraint("Constraint1", new string[] {
                        "Id"}, true)});
            this.dataTable6.PrimaryKey = new System.Data.DataColumn[] {
        this.dataColumn48};
            this.dataTable6.TableName = "Cliente";
            // 
            // dataColumn48
            // 
            this.dataColumn48.AllowDBNull = false;
            this.dataColumn48.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn48.ColumnName = "Id";
            this.dataColumn48.DataType = typeof(int);
            // 
            // dataColumn49
            // 
            this.dataColumn49.ColumnName = "Cédula";
            // 
            // dataColumn50
            // 
            this.dataColumn50.AllowDBNull = false;
            this.dataColumn50.ColumnName = "Nombres";
            // 
            // dataColumn51
            // 
            this.dataColumn51.ColumnName = "Apellidos";
            // 
            // dataColumn52
            // 
            this.dataColumn52.ColumnName = "Teléfono";
            // 
            // dataColumn53
            // 
            this.dataColumn53.ColumnName = "Correo";
            // 
            // dataColumn54
            // 
            this.dataColumn54.ColumnName = "Dirección";
            // 
            // bsProductos
            // 
            this.bsProductos.DataMember = "Producto";
            this.bsProductos.DataSource = this.dsProductos;
            // 
            // dataColumn58
            // 
            this.dataColumn58.ColumnMapping = System.Data.MappingType.Hidden;
            this.dataColumn58.ColumnName = "NA";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(635, 365);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMain";
            this.Text = "Sistema de Ventas";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dsProductos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.tblProductos)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsProductos)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem catalogoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem empleadosToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem salirToolStripMenuItem;
        private System.Data.DataSet dsProductos;
        private System.Data.DataTable tblProductos;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Windows.Forms.BindingSource bsProductos;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn13;
        private System.Data.DataColumn dataColumn14;
        private System.Data.DataColumn dataColumn15;
        private System.Data.DataColumn dataColumn16;
        private System.Data.DataColumn dataColumn31;
        private System.Data.DataColumn dataColumn30;
        private System.Data.DataTable dataTable2;
        private System.Data.DataColumn dataColumn17;
        private System.Data.DataColumn dataColumn18;
        private System.Data.DataColumn dataColumn19;
        private System.Data.DataColumn dataColumn20;
        private System.Data.DataColumn dataColumn21;
        private System.Data.DataColumn dataColumn22;
        private System.Data.DataColumn dataColumn23;
        private System.Data.DataColumn dataColumn24;
        private System.Data.DataTable dataTable3;
        private System.Data.DataColumn dataColumn25;
        private System.Data.DataColumn dataColumn26;
        private System.Data.DataColumn dataColumn27;
        private System.Data.DataColumn dataColumn28;
        private System.Data.DataColumn dataColumn29;
        private System.Windows.Forms.ToolStripMenuItem facturaciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem nuevaFacturaToolStripMenuItem;
        private System.Data.DataTable dataTable4;
        private System.Data.DataColumn dataColumn32;
        private System.Data.DataColumn dataColumn33;
        private System.Data.DataColumn dataColumn34;
        private System.Data.DataColumn dataColumn35;
        private System.Data.DataColumn dataColumn36;
        private System.Data.DataTable dataTable5;
        private System.Data.DataColumn dataColumn37;
        private System.Data.DataColumn dataColumn38;
        private System.Data.DataColumn dataColumn39;
        private System.Data.DataColumn dataColumn40;
        private System.Data.DataColumn dataColumn41;
        private System.Data.DataColumn dataColumn42;
        private System.Data.DataColumn dataColumn43;
        private System.Data.DataColumn dataColumn44;
        private System.Data.DataColumn dataColumn45;
        private System.Data.DataColumn dataColumn46;
        private System.Data.DataColumn dataColumn47;
        private System.Windows.Forms.ToolStripMenuItem FacturasToolStripMenuItem;
        private System.Data.DataTable dataTable6;
        private System.Data.DataColumn dataColumn48;
        private System.Data.DataColumn dataColumn49;
        private System.Data.DataColumn dataColumn50;
        private System.Data.DataColumn dataColumn51;
        private System.Data.DataColumn dataColumn52;
        private System.Data.DataColumn dataColumn53;
        private System.Data.DataColumn dataColumn54;
        private System.Windows.Forms.ToolStripMenuItem clientesToolStripMenuItem;
        private System.Data.DataColumn dataColumn57;
        private System.Data.DataColumn dataColumn55;
        private System.Data.DataColumn dataColumn56;
        private System.Data.DataColumn dataColumn58;
    }
}

