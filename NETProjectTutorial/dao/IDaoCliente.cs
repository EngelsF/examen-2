﻿using NETProjectTutorial.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NETProjectTutorial.dao
{
    interface IDaoCliente: IDao<Cliente>
    {
        Cliente findById(int id);
        Cliente findByLastname(string cedula);
        List<Cliente> findbyLastname(string lastname);
    }
}
