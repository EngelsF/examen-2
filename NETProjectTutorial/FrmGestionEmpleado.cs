﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NETProjectTutorial
{
    public partial class FrmGestionEmpleado : Form
    {
        private DataSet dsEmpleados;
        private BindingSource bsEmpleados;


      
        public FrmGestionEmpleado()
        {
            InitializeComponent();
            bsEmpleados = new BindingSource();
        }

        public DataSet DsEmpleados
        {
            
            set
            {
                dsEmpleados = value;
            }
        }

        private void FrmGestionEmpleado_Load(object sender, EventArgs e)
        {
            bsEmpleados.DataSource = dsEmpleados;
            bsEmpleados.DataMember = dsEmpleados.Tables["Empleado"].TableName;
            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;
        }

        private void btnnuevo_Click(object sender, EventArgs e)
        {
            FrmEmpleado fe = new FrmEmpleado();
            fe.TblEmpleados = dsEmpleados.Tables["Producto"];
            fe.DsEmpleados = dsEmpleados;
            fe.ShowDialog();
        }
    }
}
